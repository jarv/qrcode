package main

import (
	"embed"
	"flag"
	"fmt"
	"html/template"
	"log/slog"
	"net/http"
	"net/url"
	"os"
	"path"
	"path/filepath"
	"time"

	"github.com/sqids/sqids-go"
	"github.com/yeqown/go-qrcode/v2"
	"github.com/yeqown/go-qrcode/writer/standard"
)

const (
	qrLogoFile = "qrLogoDir/gitlab-logo.png"
)

var (
	addr     = flag.String("addr", "localhost:8740", "http service address")
	qrImgDir = flag.String("qrImgDir", "qrImgDir", "directory to store QR images")
	logger   = slog.New(slog.NewJSONHandler(os.Stderr, nil))

	templates = template.Must(template.ParseFS(tmplFiles, "tmpl/*.tmpl"))
	//go:embed static
	staticFiles embed.FS

	//go:embed tmpl/*.tmpl
	tmplFiles embed.FS

	//go:embed qrLogoDir/*
	qrLogoFiles embed.FS

	idGen *sqids.Sqids
)

func main() {
	flag.Parse()

	var err error
	idGen, err = sqids.New()
	if err != nil {
		panic(err)
	}

	if err := os.MkdirAll(*qrImgDir, os.ModePerm); err != nil {
		panic(err)
	}

	ticker := time.NewTicker(15 * time.Minute)
	defer ticker.Stop()
	go deleteOldPNGFiles(*qrImgDir, ticker.C)

	mux := http.NewServeMux()

	// QR Code images
	qrFilesServer := http.FileServer(http.Dir(*qrImgDir))
	mux.Handle("GET /q/", http.StripPrefix("/q/", qrFilesServer))

	// GET Static Assets
	mux.Handle("GET /static/", http.FileServer(http.FS(staticFiles)))

	// Root HTML
	mux.HandleFunc("GET /", func(w http.ResponseWriter, r *http.Request) {
		if err := templates.ExecuteTemplate(w, "qrCodeCreate.html.tmpl", nil); err != nil {
			http.Error(w, "server error", http.StatusInternalServerError)
			slog.Error("Error executing template", "err", err)
			return
		}
	})

	// POST handler for creating a QR Code
	mux.HandleFunc("POST /c", func(w http.ResponseWriter, r *http.Request) {
		qrCodeDestURL := r.PostFormValue("qrCodeDestURL")
		url, err := url.Parse(qrCodeDestURL)
		if qrCodeDestURL == "" || err != nil {
			http.Error(w, "Unable to read URL", http.StatusInternalServerError)
			slog.Error("Unable to read URL", "qrCodeDestURL", qrCodeDestURL)
			return
		}

		strID, err := genQRImg(url)
		if err != nil {
			http.Error(w, "Unable to generate QR Code", http.StatusInternalServerError)
			slog.Error("Unable to generate QR Code", "err", err)
			return
		}

		qrCodeURL := genQRCodeURL(r, strID)

		slog.Info("Generated QR Code", "qrCodeURL", qrCodeURL.String())
		d := struct {
			QRCodeURL string
		}{
			qrCodeURL.String(),
		}
		if err := templates.ExecuteTemplate(w, "qrCode.html.tmpl", d); err != nil {
			http.Error(w, "server error", http.StatusInternalServerError)
			slog.Error("Error executing template", "err", err)
			return
		}
	})

	server := &http.Server{
		Addr:              *addr,
		ReadHeaderTimeout: 3 * time.Second,
		Handler:           mux,
	}

	logger.Info("Server started", "addr", "http://"+*addr)
	if err := server.ListenAndServe(); err != nil {
		logger.Error("Unable to setup listener", "err", err)
		os.Exit(1)
	}
}

func genQRImg(url *url.URL) (string, error) {
	ts := uint64(time.Now().UnixMilli())
	strID, err := idGen.Encode([]uint64{ts})
	if err != nil {
		slog.Error("Unable to encode ts", "ts", ts)
		panic(err)
	}

	qrImgFile := strID + ".png"
	imgPath := path.Join(*qrImgDir, qrImgFile)

	// If the image file exists return it
	_, err = os.Stat(imgPath)
	if err == nil {
		return qrImgFile, nil
	}

	qrc, err := qrcode.NewWith(url.String(),
		qrcode.WithEncodingMode(qrcode.EncModeByte),
		qrcode.WithErrorCorrectionLevel(qrcode.ErrorCorrectionQuart),
	)
	if err != nil {
		return "", err
	}

	tempFile, err := os.CreateTemp("", "qrLogo_*.png")
	if err != nil {
		return "", err
	}
	// Write the content of the embedded file to the temporary file
	fileContent, err := qrLogoFiles.ReadFile(qrLogoFile)
	if err != nil {
		return "", err
	}

	_, err = tempFile.Write(fileContent)
	if err != nil {
		return "", err
	}
	defer os.Remove(tempFile.Name())

	qrWriter, err := standard.New(imgPath,
		standard.WithLogoImageFilePNG(tempFile.Name()),
		standard.WithLogoSizeMultiplier(2),
		standard.WithFgColorRGBHex("#2f2a6b"),
		//		standard.WithCircleShape(),
		standard.WithBgTransparent(),
		standard.WithBorderWidth(4),
	)
	if err != nil {
		return "", err
	}

	if err = qrc.Save(qrWriter); err != nil {
		return "", err
	}

	return strID, nil
}

func genQRCodeURL(r *http.Request, strID string) *url.URL {
	scheme := r.Header.Get("X-Forwarded-Proto")
	if scheme == "" {
		scheme = "http"
	}

	host := r.Header.Get("X-Forwarded-Host")
	if host == "" {
		host = r.Host
	}

	return &url.URL{
		Scheme: scheme,
		Host:   host,
		Path:   "/q/" + strID + ".png",
	}
}

func deleteOldPNGFiles(imgDir string, tick <-chan time.Time) {
	for {
		<-tick
		slog.Info("Checking for files to cleanup")
		currentTime := time.Now()
		cutoffTime := currentTime.Add(-2 * time.Hour)
		pngFiles, err := filepath.Glob(filepath.Join(imgDir, "*.png"))
		if err != nil {
			slog.Error("Error listing old png files", "err", err)
			continue
		}

		for _, file := range pngFiles {
			fileInfo, err := os.Stat(file)
			if err != nil {
				fmt.Println("Error:", err)
				continue
			}

			if fileInfo.ModTime().Before(cutoffTime) {
				err := os.Remove(file)
				if err != nil {
					slog.Error("Error deleting file", "err", err)
				} else {
					slog.Info("Deleted old file", "file", file)
				}
			}
		}
	}
}
