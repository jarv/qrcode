package main

import (
	"fmt"

	"github.com/sqids/sqids-go"
)

func main() {
	s, _ := sqids.New(sqids.Options{
		Alphabet:  "ACDEFGHJKLMNPQRTUVWXYZ23456789",
		MinLength: 4,
	},
	)
	id, _ := s.Encode([]uint64{1}) // "86Rf07"
	fmt.Println(id)

	id1, _ := s.Encode([]uint64{2}) // "86Rf07"
	fmt.Println(id1)
	id2, _ := s.Encode([]uint64{3}) // "86Rf07"
	fmt.Println(id2)
	id3, _ := s.Encode([]uint64{50000}) // "86Rf07"
	fmt.Println(id3)

	numbers := s.Decode(id) // [1, 2, 3]
	fmt.Println(numbers)

}
