module gitlab.com/jarv/qrcode

go 1.22

require (
	github.com/google/uuid v1.6.0
	github.com/gorilla/websocket v1.5.1
	github.com/sqids/sqids-go v0.4.1
	github.com/yeqown/go-qrcode/v2 v2.2.2
	github.com/yeqown/go-qrcode/writer/standard v1.2.2
)

require (
	github.com/alexedwards/scs/v2 v2.7.0 // indirect
	github.com/fogleman/gg v1.3.0 // indirect
	github.com/golang/freetype v0.0.0-20170609003504-e2365dfdc4a0 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/stretchr/testify v1.8.4 // indirect
	github.com/yeqown/reedsolomon v1.0.0 // indirect
	golang.org/x/image v0.15.0 // indirect
	golang.org/x/net v0.17.0 // indirect
)
