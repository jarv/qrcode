/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./**/*tmpl",
  ],
  theme: {
    extend: {},
  },
  plugins: [
      require('@tailwindcss/aspect-ratio'),
      require('@tailwindcss/forms'),
  ],
}
