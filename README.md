# QR Code Generator

Simple app that generates a QR code

<img width=200 src="/uploads/dc66f4596309ac51b9dd08b6683ac24d/image.png" alt="">

Built with:

- [Go](https://go.dev/)
- [tailwindcss](https://tailwindcss.com/)
- [htmx](https://htmx.org/)
- [go qrcode](https://github.com/yeqown/go-qrcode)

## Local development

- Install dependencies using `mise install`
- Run the app with `go run app.go`, or watch for changes with `./bin/watch`

## Assets

Assets are checked into the repo, to rebuild

- `./bin/rebuild-assets`
